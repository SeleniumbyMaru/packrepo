package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;

import PFPProject.base.BaseTest;
import Pack.util.ConStants;
import pages.LaunchPage;

public class LoginTest extends BaseTest {

	@Test
	public void testlogin() {

		eTest = eReport.startTest("LoginTest");

		eTest.log(LogStatus.INFO, "Login Test has started");

		OpenBrowser(ConStants.BROWSER_TYPE);

		LaunchPage launchpage = new LaunchPage(driver, eTest);

		PageFactory.initElements(driver, launchpage);

		boolean loginstatus = launchpage.gotoLoginPage();

		if (loginstatus) {

			// Pass the test case //

			reportpass("LoginTest TestCase passed");

		} else {

			// Fail the test case //

			reportfail("LoginTest TestCase Failed");
		}

	}

	@AfterMethod
	public void testclosure() {

		if (eReport != null) {
			eReport.endTest(eTest);
			eReport.flush();
		}

		if (driver != null) {

			driver.quit();
		}
	}

}
