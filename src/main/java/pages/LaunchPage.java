package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Pack.util.ConStants;
import base.BasePage;

public class LaunchPage extends BasePage {
	
	
	//WebElements of Launch Page
	
	@FindBy(className="zgh-megaMenu zgh-products")
	public WebElement Products;
	
	
	@FindBy(linkText="Customers")
	public WebElement CustomersName;
	
	@FindBy(linkText="Support")
	public WebElement SupportColumn;
	
	@FindBy(linkText="About Us")
	public WebElement AboutUscolumn;
	
	@FindBy(linkText="LOGIN")
	public WebElement Loginbutton;
		
	//Any other Locators
	
	//Constructor code here
	
	public LaunchPage(WebDriver driver, ExtentTest eTest) {
		
		this.driver =driver;
		
		this.eTest= eTest;
		
		
	}
	
	// Reusable methods of Launch Page
	
	public boolean gotoLoginPage() {
		
		driver.get(ConStants.APP_URL);
		
		eTest.log(LogStatus.INFO, "Application URL "+ ConStants.APP_URL+"got Opened");
		
		Loginbutton.click();
		
		eTest.log(LogStatus.INFO, "Login option got clicked");
		
		LoginPage loginpage = new LoginPage(driver,eTest);
		
		PageFactory.initElements(driver, loginpage);
		
		boolean loginstatus =loginpage.doLogin();
		
		return loginstatus;
		
		
		// selenium Automation code //
		
		//Any other reusable methods //
		
	}

}
