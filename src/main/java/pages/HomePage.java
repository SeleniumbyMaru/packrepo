package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.relevantcodes.extentreports.ExtentTest;

import base.BasePage;

public class HomePage extends BasePage {
		
	@FindBy(css="span[class^='zicon-apps-calendar']")
	public WebElement Calendar;
	
	@FindBy(css="span[class^='zicon-apps-chat']")
	public WebElement CliQ;
	
	@FindBy(css="span[class^='zicon-apps-connect']")
	public WebElement Connect;
	
	@FindBy(css="span[class^='zicon-apps-crm']")
	public WebElement CRM;
	
	//Any other locators copy here //
	
	// Constructor code here
	
	public HomePage(WebDriver driver, ExtentTest eTest) {
		
		this.driver=driver;
		this.eTest=eTest;
	}
	
	//Re usable methods for verifying whether user got successfully logged in //
	
	public boolean verifyDisplayOfHomePage() {
		
		return (isElementPresent(CRM));
	}
	
	// re usable methods for HomePage //
	
	

}
