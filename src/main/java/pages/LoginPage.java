package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Pack.util.ConStants;
import base.BasePage;

public class LoginPage extends BasePage {

	// WebElements of LoginPage here //
	
	@FindBy(id="lid")
	public WebElement EmailField;
	
	@FindBy(id="pwd")
	public WebElement PasswordField;
	
	
	@FindBy(id="signin_submit")
	public WebElement SignInButton;
	
	// Any other Locators //
	
	//Constructor Code here //
	
	public LoginPage(WebDriver driver, ExtentTest eTest) {
		
		this.driver=driver;
		this.eTest=eTest;
	}
	
	// reusable methods here //
	
	public boolean doLogin() {
		
		EmailField.sendKeys(ConStants.USERNAME);
		eTest.log(LogStatus.INFO, "Username got entered into the Email address Field on Login Page");
		PasswordField.sendKeys(ConStants.PASSWORD);
		eTest.log(LogStatus.INFO, "Password got entered successfuly");
		SignInButton.click();
		eTest.log(LogStatus.INFO, "Sign in button got Clicked");
		
		HomePage homepage = new HomePage(driver,eTest);
		
		PageFactory.initElements(driver, homepage);
		
		boolean loginstatus= homepage.verifyDisplayOfHomePage();
		
		return loginstatus;
		
		
	// Automation code write here //	
		
	}
	
	// Any other reusable methods //
}
